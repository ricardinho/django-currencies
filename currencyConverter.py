#!/env/python
#Converts currencies according to the bank of Israel rates.
#Written by Ricardo Moreno
#May 2012

import os
import sys
import urllib
import tempfile
from decimal import getcontext, Decimal
from xml.dom.minidom import parseString
from datetime import timedelta, datetime


LINK = 'http://www.bankisrael.gov.il/currency.xml'
TIME_DELTA = timedelta(hours=1)
CACHE_FILE_NAME = 'converter_cache.txt'


def get_with_cache(url, time_delta, file_name):
    filepath = os.path.join(tempfile.gettempdir(), file_name)
    cache = False
    if not os.path.isfile(filepath):
        f = open(filepath, 'w+')
        f.close()
        cache = True

    delta = datetime.now() - datetime.fromtimestamp(os.path.getmtime(filepath))

    if (not cache) and delta > time_delta:
        cache = True

    if cache:
        try:
            conversions = urllib.urlopen(LINK)
            stringer = conversions.read()
            doc = parseString(stringer)
            f = open(filepath, 'w')
            f.write(stringer)
            f.close()
        except:
            f = open(filepath, 'r')
            doc = parseString(f.read())
            f.close()

    else:
        f = open(filepath, 'r')
        doc = parseString(f.read())
        f.close()

    return doc


def convert(amount, originalCurrency, targetCurrency):
    u'''Converts the given amount from the original currency to the target
    currency'''
    getcontext().prec = 5
    currencies = (originalCurrency, targetCurrency)
    conversions = []
    doc = get_with_cache(LINK, TIME_DELTA, CACHE_FILE_NAME)
    for currency in currencies:
        if currency == 'ILS':
            conversions.append(Decimal(1))
        else:
            elements = [node for node in \
                    doc.getElementsByTagName('CURRENCYCODE') if
                    node.firstChild.nodeValue == currency][0].parentNode
            num = Decimal(elements.getElementsByTagName('RATE')[0]. \
                    firstChild.nodeValue) / \
                        Decimal(elements.getElementsByTagName('UNIT')[0]. \
                        firstChild.nodeValue)
            conversions.append(num)

    result = Decimal(amount) * conversions[0] / conversions[1]

    return result


def exchange_rate():
    return convert(1, "USD", "EUR")


if __name__ == '__main__':
    print convert(sys.argv[1], sys.argv[2], sys.argv[3])
