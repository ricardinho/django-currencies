"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from currencies.models import Currency


class CurrencyTest(TestCase):
    fixtures = ['default_currencies.json']

    def test_equivalence(self):
        c1 = Currency.objects.get(code='USD')
        c2 = Currency.objects.get(code='GBP')
        self.assertEqual(c1.convertToMe(c1, 1), 1)
