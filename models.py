from django.db import models
import currencyConverter as cc


class Currency(models.Model):
    code = models.CharField(max_length=3, primary_key=True)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return "{0}".format(self.code)


    def convertToMe(self, currency, amount):
        return cc.convert(amount, currency.code, self.code)


    class Meta:
        verbose_name_plural = "currencies"
